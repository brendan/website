---
---

# Groans

These are the things that make me groan/are a pet peeve of mine. Along with self-diagnosed [misophonia](https://en.wikipedia.org/wiki/Misophonia), I also get inordinately annoyed at things that shouldn't annoy me. And a buddy asked me for a ranked list...so here we are.

## Ranked List

1. Chewing in my ear
1. Chewing _near_ me
1. Other totally normal human sounds (heavy breathing, slurping coffee)
1. Cutting nails in a public/shared space
1. Bubble Tea: Why does it even exist? It might be the grossest food in the world, honestly. Slimy balls that I ingest? No, thank you.
1. People who go around you while driving because (assumably) they want to go faster than you but then actually objectively go slower because you had on cruise control
1. People who recline their airline seat during a meal
1. When you lose something and a person says, "Well, where was the last place you had it?"

## Other things that may make a ranking at some point
