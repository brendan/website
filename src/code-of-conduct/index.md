---
title: Code of Conduct
---

# Code of Conduct

`olearycrew` _code of conduct_ **tl;dr**:

> Be respectful of other people, respectfully ask people to stop if you are bothered, and if you can't resolve an issue, contact staff. If you are a problem, it will be apparent, and you'll be asked to leave.

This is a living ' code of conduct based on previous [excellent work by others](https://indieweb.org/code-of-conduct-examples). This code of conduct applies to all `boleary.dev` and `olearycrew` spaces both online and off, including my Twitch stream, my Twitter DMs, and really anytime we're hanging out.

## Respect

`olearycrew` and `boleary.dev` are intentionally positive communities that recognize and celebrate the creativity and collaboration of independent creators (and independence) and the diversity of people, cultures, and opinions they bring to our community. This is firmly based on my own personal belief that [Everyone Can Contribute](https://www.youtube.com/watch?v=V2Z1h_2gLNU).

`olearycrew` spaces are an inclusive environment, based on treating all individuals respectfully, regardless of gender (including transgender status), sexual orientation, age, disability, medical conditions, nationality, ethnicity, religion (or lack thereof), physical appearance, politics, ideology (real-world or tech), or software preferences.

We value respectful behavior above individual opinions.

Respectful behavior includes:

- Be considerate, kind, constructive, and helpful.
- Avoid demeaning, discriminatory, harassing, hateful, or physically threatening behavior, speech, and imagery.
- If you're not sure, ask someone instead of assuming.

`olearycrew` prioritizes marginalized people's safety over privileged people's comfort. `olearycrew` organizers reserve the right not to act on complaints regarding:

- 'Reverse' -isms, including 'reverse racism,' 'reverse sexism,' and 'cisphobia.'
- Reasonable communication of boundaries, such as "leave me alone," "go away," or "I'm not discussing this with you."
- Communicating in a 'tone' you don't find congenial
- Criticizing racist, sexist, cissexist, or otherwise oppressive behavior or assumptions

<small>(Thanks to XOXO for providing the above paragraph and list on marginalized people's safety with a [https://2018.xoxofest.com/conduct CC-BY license])</small>

## Resolve Peacefully

We believe peer-to-peer discussions, feedback, corrections can help build a stronger, safer, and more welcoming community.

If you see someone behaving disrespectfully, you are encouraged to discourage them from such behavior respectfully. Expect that others in the community wish to help keep the community respectful and welcome your input.

If you experience disrespectful behavior and feel in any way unable to respond or resolve it respectfully (for any reason), please immediately bring it to the attention of an organizer. We want to hear from you about anything that you feel is disrespectful, threatening, or just icky in any way. We will listen and work to resolve the matter.

## Apologize for Mistakes

Should you catch yourself behaving disrespectfully or be confronted as such, own up to your words and actions, and apologize accordingly. No one is perfect, and even well-intentioned people make mistakes. What matters is how you handle them and avoid repeating them in the future.

## Consequences

Suppose the organizers determine that an event participant is behaving disrespectfully. In that case, the organizers may take any action they deem appropriate, up to and including expulsion and exclusion from the event without warning or refund.

As organizers, we will seek to resolve conflicts peacefully and positively for the community. We can't foresee every situation, however, and thus if in the organizers' judgment, the best thing to do is to ask a disrespectful individual to leave, we will do so.

## Resolving an Issue

If you believe you're experiencing practices at an `olearycrew` event or space which doesn't meet the policies below, or if you feel you are being harassed, please immediately contact the organizer(s) or designated code of conduct responders for the event.

If you believe you're experiencing practices in chat or at an event that don't meet the policies above, please immediately contact any ally you may know. Anyone in the community should feel empowered to resolve issues and uphold our high standards.

However, if you feel the issue requires escalation to a community organizer, please contact:

- Brendan O'Leary US/Eastern
  - <code>olearycrew</code> in Disqus threads
  - Contact [olearycrew on Twitter](https://twitter.com/olearycrew)

## License

This Code of Conduct is inspired by the CC0 Code of Conduct from [IndieWeb](https://indieweb.org/code-of-conduct). Thus, this Code of Conduct is also released to the public domain, according to CC0.

## Open Code of Conduct

You can re-use the `olearycrew` code-of-conduct for your community by:

- Copy/paste the whole thing (the CC0 license lets you do this)
- Replace "olearycrew" with the name of your organization
- Make any organization-specific customizations you think are needed
- You may attribute indieweb.org/code-of-conduct and boleary.dev/code-of-conduct as a source if you wish
- Consider also licensing your Code of Conduct with the CC0 license so others may easily re-use it as well

## Signed

Organizers:

- [Brendan O'Leary](https://twitter.com/olearycrew), 2020-06-22. Updatred 2022-02-05

## Comments

<CommentoPageView/>
