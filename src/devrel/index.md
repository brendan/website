# Developer Relations (DevRel)

## What is DevRel?
Developer Relations - also called "DevRel" - is the practice of connecting with communities of developers to help them understand, adopt, and embrace a technology stack, tool, or methodology.

## Where can I learn more?
Here are some more resources for learning about DevRel, developer advocacy, and community.

### People to Follow
- [Mary Thengvall](https://www.marythengvall.com)
- [Jono Bacon](https://twitter.com/jonobacon?s=21)
- [Tessa Kriesel](https://twitter.com/tessak22?s=21)
- [Stephanie Morillo](https://www.stephaniemorillo.co)

### Books
- [The Business Value of Developer Relations](https://www.persea-consulting.com/book)
- [The Developer's Guide to Content Creation](https://www.stephaniemorillo.co/product-page/the-developer-s-guide-to-content-creation)
- [People Powered](https://www.jonobacon.com/books/peoplepowered/)
- [Developer Relations: How to build and grow a successful developer program](https://www.devrelbook.com)
- [Badass: Making Users Awesome](https://www.amazon.com/Badass-Making-Awesome-Kathy-Sierra/dp/1491919019)

### Other Resources
- [DevRel Collective](https://devrelcollective.fun)
- [DevRel Weekly](https://devrelweekly.com)
- [DevRelCon](https://www.youtube.com/channel/UCabc3QtCLKsNeTOx9cqDSlQ)
- [Devocate](https://www.devocate.com)
- [Defining a career path for Developer Relations](https://slack.engineering/defining-a-career-path-for-developer-relations/)
- [Measuring Developer Relations](https://www.youtube.com/watch?v=NRTUA0cSMnQ)
- [DevRel Resources](https://gist.github.com/andypiper/7fdb73f9713b7e1f7e5e7ac373f635cb)