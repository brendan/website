---
meta:
  - name: "twitter:title"
    content: "It's time to update your Chrome"
  - name: "twitter:description"
    content: "Someone who knows about tech sent you this because they want to make sure you're safe.  Isn't that nice?"
  - name: "og:title"
    content: "It's time to update your Chrome"
  - name: "og:description"
    content: "Someone who knows about tech sent you this because they want to make sure you're safe.  Isn't that nice?"
---

## Updating Google Chrome
You should update anytime you see a yellow or red "Update" in the upper right corner of your browser.  To check for updates manually:

1. Open Chrome
1. In the address bar, type `chrome://settings/help`
1. Chrome will start checking for updates
1. If one is available, you'll see the message 
   - `Nearly up to date! Relaunch Chrome to finish updating.`
1. Click **Relaunch**
1. 🎉