---
title:  "What's in a name?"
date:   2020-07-08 05:00:00
author: brendan
type: post
blog: true
excerpt: > 
  Yes, my last name has an apostrophe in it.  Yes, that is a VALID name.
meta:
  - name: "twitter:title"
    content: "What's in a name?"
  - name: "twitter:description"
    content: "Yes, my last name has an apostrophe in it.  Yes, that is a VALID name."
  - name: "og:title"
    content: "What's in a name?"
  - name: "og:description"
    content: "Yes, my last name has an apostrophe in it.  Yes, that is a VALID name."
---

## My Last Name is valid

My last name is `O'Leary`.  Spelling it over the phone is fun..."That's O, apostrophe, capital L, e a r y."  Despite what many developers think, this is an entirely valid last name.  I've dealt with folks too lazy to deal with a `'` in a last name my whole life, which is one thing.  I get it, SQL can be hard.  But then many "solve" this problem through form validation that says "please enter a valid last name."  Talk about *invalidating* someone's experience in life.

It's not the biggest deal, but I share because there are a LOT of less privileged people than me that deal with lots of ways that developers don't include them.  Names, for instance, [have a lot a variation](https://www.kalzumeus.com/2010/06/17/falsehoods-programmers-believe-about-names/) but it doesn't end there.  [Accessibility](https://brailleworks.com/accessibility-matters/) is critical.  Considering how your [terminology](/blog/2020-06-10-i-was-wrong-about-git-master.html) may impact other people is essential. Without diversity of thought in your development teams, you risk repeatedly causing this same problem for people with more pressing concerns.  So...don't be lazy.

## I swear it's not about me
<center>
<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Wow, that sucks. I'm sorry you have to deal with this all the time.</p>&mdash; William Chia (@TheWilliamChia) <a href="https://twitter.com/TheWilliamChia/status/1280716222078816256?ref_src=twsrc%5Etfw">October 16, 2014</a></blockquote>
<figcaption class="image-caption">
    A good friend expresses his empathy for my situation.
</figcaption>
</center>

When I shared one of these examples, my friend William expressed what I'd expect from such an awesome person: empathy.  And honestly, I appreciate that empathy.  But I don't share these cases just for me.  And I don't even share them for all my Irish brothers and sisters with `O'`.  And I don't just share them for everyone with special characters in their last name, or no last name, or a name that doesn't fit into the confines of "first name / middle name / last name."

It's mostly just annoying for me...I have the perspective that there are way more challenging things that folks deal with when apps are written without considering people like them. But I bring awareness around it for EXACTLY that reason. If we as a software industry can't get these little, easy things right, how will we ever bring genuine empathy to what we build?! There are a lot of people that don't look, think, behave as we do. If we want to be successful, we're hoping that a LOT of those people sign up.

> Everyone Can Contribute

So, not only is it the right thing to do for humanity, but it's the right thing to do selfishly for our success to try our best to include everyone in our design, everyone in our use cases.

## Examples
If you follow me on [Twitter](htttps://twitter.com/olearycrew), you'll sometimes see me post this when I find a new place my last name doesn't work.  I hope to encourage people to consider those who are different than themselves who will one day (hopefully, if you're successful) be users of their app.

## Moving forward
This is an important enough topic for me that I don't want it to be just one blog post.  As such, I've also published this post at [`boleary.dev/thoughts/name`](/thoughts/name) and will keep THAT page up to date with more things I find similar to this name issue to try and raise awareness to developers about how we can do things better for everyone.

## The twitter feed

To see those examples, you can search for [`#oapostrophe from:@olearycrew`](https://twitter.com/search?q=%23oapostrophe%20from%3A%40olearycrew&src=typed_query) on Twitter, or check out my collection below.

<a class="twitter-timeline" href="https://twitter.com/olearycrew/timelines/1280856165358985217?ref_src=twsrc%5Etfw">oapostrophe - Curated tweets by olearycrew</a> 