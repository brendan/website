---
title:  "How I Slack"
date:   2020-05-28 05:00:00
tags: 
  - useless-is-not-worthles
  - learning
  - slack
#image: /img/blog/pihole1.png
type: post
blog: true
author: brendan
excerpt: > 
  Having used Slack for some time now, I've developed several practices that have helped me deal with a sizeable Slack environment.  In this post, I'll share those with you!
---

Since I was an early adopter of [Slack](https://slack.com/), I've been using Slack for almost half of my career at this point.  I've always been a fan of chat as a way to collaborate with friends and colleagues.  Before Slack, it was [HipChat](https://en.wikipedia.org/wiki/HipChat) - which I brought into my first role out of college.  And in college, even, chat was a big part of my life.  It probably dates me, but a little thing called [AOL Instant Messager](https://en.wikipedia.org/wiki/AIM_(software)) (AIM) was a big part of college life when I was there.  Yes, Facebook was still ".edu addresses" only, but I graduated years before Messenger was released.

<center>
<blockquote class="twitter-tweet"><p lang="en" dir="ltr">let the rain fall down --- Hilary Duff</p>&mdash; your away message (@YourAwayMessage) <a href="https://twitter.com/YourAwayMessage/status/522580505544638465?ref_src=twsrc%5Etfw">October 16, 2014</a></blockquote>
<figcaption class="image-caption">
    In the AIM days, you were defined by how witty your away messages were.
</figcaption>
</center>

Having used Slack for some time now, I've developed several practices that have helped me deal with a sizeable Slack environment.  I've promised my team at [GitLab](https://about.gitlab.com/) for some time that I'd write about how I manage my life in Slack. I wouldn't dare call these practices "best," but they are what has worked for me, and most of this will be just _in my opinion_...but here goes nothing.

> If you know your way around Slack somewhat already, you might just want the [tl;dr](#tl-dr-my-slack-setup) version below.

## Basics

When starting with Slack, the interface can be overwhelming. The sheer number of options and mental models needed to get up to speed can be tough to reason.  However, at its core, Slack is nothing if not a chat app.  And those chats have different contexts in which they can happen.

Just like in an office, you could have a meeting with everyone in it and contribute at the same time, or a smaller meeting with only a few folks or just a 1:1 with your boss or a direct report - Slack has many similar modes of communication. Two basic constructs here are direct messages (DMs) and channels.  

### 📺 Channels

Channels allow many people to contribute to a conversation or a decision. They are written as `#` followed by a channel name.  Some channels can be public. For instance, many Slack instances have a `#general` where *everyone* can see common messages.  But channels can also be private, only allowing invited team members to view and participate.  In channels, you can "@ mention" other team members to get their attention or even add them to a channel they might not already be in.

### 🗣️ Direct Messages

Direct messages are more...well...direct.  They might be a 1:1 message with a colleague where you can discuss items in private.  Here "@ mentioning" other team members or channel names won't alert them, giving natural links for your conversation partner to follow.  Direct messages can also have multiple members - kind of like a group text message. But these have less functionality than private channels do and thus should be avoided.

## 🆕 A new 'office'

While the office analogy can help in getting started, the differences from a typical office environment make Slack shine.  Unlike meetings that happen once and then are lost to the poorly taken notes, Slack harnesses the power of synchronous AND asynchronous communication at the right times.  There may be a flurry of activity where folks can chime in real-time, but that activity is preserved for later.  If the people in the discussion - or people who weren't able to be there at the same time - want to remember or learn how the conversation went and how a final decision was made, the entire history is there.

This blend of asynchronous and synchronous communication is what makes Slack special.  But as with any software tool, the tool alone won't solve all your problems.

## 🤔 Slack Mentality

For many people that are new to Slack, the closest analog to it they may have had before is e-mail (*shutters*).  This mental model can be a substantial limiting factor for one of the most important things to getting real value out of Slack - having the right mentality.

E-mail is strictly asynchronous communication.

<figcaption class="image-caption">
<i>
Actually my dad used to use e-mail as a kind of chat in meetings that were boring. He would send subject-line-only chat messages to colleagues in the room who could reply with the same.   Sorry for putting you on blast dad.
</i>
</figcaption>

Also, frequently (at least it feels) e-mail requires a response from the sender...or at the least that the sender read the entire message to see if they are CC-ed to give an opinion or just as an "FYI."  Slack solves both of these issues.  An instant chat with history enables synchronous and asynchronous communication for those not "there" at the time to read the entire conversation.  Also, most channels you're in should be seen as the "FYI only" - tagging team members allows the sender to make it very clear when expecting a response from the receiver(s).

In a sizeable Slack environment, though, this can get overwhelming.  Instead of being freed from being tied to "Inbox zero" in your e-mail client, it can feel like you have *dozens* of inboxes (channels) - many of which aren't _yours_.

It is then that I often tell people to take [some advice](https://slack.com/help/articles/218551977-Reduce-noise-in-Slack) from Marie Kondo.  If the channel doesn't spark joy,  leave it.  People shouldn't see that as rude - but just controlling your own efficiency.   However, there will always be more information to consume in a large enough Slack instance than you'll ever have time for.  So let's look at some features that help me manage that information overload...most of the time.

## 🧑‍🤝‍🧑 Some Slack features you should make friends with

Many Slack features help you account for and mitigate the problem of channel sprawl or not knowing where to put your attention.  While there is no "silver bullet" to solve the problem of information overload, hopefully playing with these features can help.  Remember that it should always be an iterative process: none of these settings are set in stone - so adjust your approach when needed.

### 🙈 Show only unread conversations
By default, Slack will show all DMs you've opened and all of your channels.  One of the strongest moves is to turn on the ability to show only conversations with unread messages:

<BlogImage
  image="/img/blog/slack-unread-only.png"
  caption="Showing only conversations with unread messages"
/>

With unread only enabled, you'll only see channels and conversations you haven't read yet.  That can help to immensely cut down on the amount of scrolling you need to do within the sidebar.  The downside is that if you want to send a message, you have to find the channel through the channel picker or keyboard shortcut.  However, there are additional options you can combine with this feature to start to provide a ranking of how critical a conversation is.

### ⭐ Starring conversations

One of my favorite features is the ability [star a conversation](https://slack.com/help/articles/201331016-Star-channels-and-direct-messages) - be they DMs or channels.  When you first launch Slack, and especially before [channel groups](), everything is a wide even mess of channels, DMs, group DMs, etc.  Starring allows me to focus on those places I know that I'm going to be needed most, or where I want to be most responsive.

I also star/unstar viciously.  Going to an event next week?  That event's channel is starred until the minute I leave, and then it's "see ya later."  For DMs, I can have even more precision.  Someone who I need to both be responsive too AND want to start conversations with often? Star and in my sidebar.  Maybe I don't start many discussions, but if one does start, I want to be all over it (say your boss's boss, for instance).  I star that conversation but close it, meaning if they ever DM me, they will be right to the top of my list.

### 🤫 Muting/Unmuting channels
Another simple but powerful option is to ["mute" a channel](https://slack.com/help/articles/204411433-Mute-channels-and-direct-messages).  When you mute a channel, you're still a member, but it is less prominent in your sidebar than other channels.  I use this for channels I want to stay up to date on, _only_ when I have time.  They aren't time-sensitive or directly related to my role, so I can follow up when I have a spare minute.

### ⏰ Slackbot reminders
Another Slack feature I couldn't live without is the ability to use [Slackbot to remind me about messages](https://slack.com/help/articles/208423427-Set-a-reminder).  Sometimes a message requires action from me, but I can't immediately deal with it.  When that happens, I use the context menu on the issue to set a reminder for an hour.  Once the hour (or day) is up, Slackbot sends me a DM reminding me about the message (and even has a snooze button 😉).

<BlogImage
  image="/img/blog/slack-reminder.png"
  caption="Have Slackbot remember things for you 🐘"
/>

### 🧵 Threads
While the other features work just for you, many features work well...IF your whole team buys into using them.

One of the most powerful of these is [threads](https://slack.com/help/articles/115000769927-Use-threads-to-organize-discussions-), which allow conversations in channels to be threaded rather than a massive stream of consciousness.  

Threads help when trying to come back and understand what's going on asynchronously if you were away at the time of the conversation.  Rather than having to scroll back in time through dozens of messages, you can read only the handful of thread-starting topics that turned into discussions.  You can then filter signal-from-noise when it comes to which threads you want to dig into and which you can leaving for "later" (read: never).

In GitLab's Slack, we even have an emoji reaction that is a spool of thread called `:threadplease:` to remind friends to please use threads.

### ALL the keyboard shortcuts
In any application I'm in, I always love keyboard shortcuts.  Whenever the industry decided to standardize on having `/` or `?` be the standard for "show me the OTHER keyboard shortcuts" is my favorite day ever.  As a software developer, I judge other software developers on their keyboard shortcuts.

Slack wins big here.  There are shortcuts for making a new message, finding a conversation by name, finding a message in a discussion based on phrase AND metadata, and much more.  I use lots of keyboard shortcuts to navigate through Slack quickly (such as going through messages or unread conversations with `alt` / `option` and the arrow keys.  

To learn more about my specific setup, see the end of the article.  To get a feel for keyboard shortcuts yourself, hit `cmd` (or `windows`) + `/` in Slack to see a whole list.  Unlike [some situations](https://xkcd.com/1205/), the time you spend now to master a keyboard shortcut will save you countless hours later.

## New features? Yas, please!

Those features I described above are tried-and-true in the sense that I've been using them for years.  But Slack has recently added some features that can - in theory - help even more with information organization.

The biggest one here is [channel sections](https://slack.com/help/articles/360043207674-Organize-conversations-with-custom-sections).  Of any new feature in the last 3 or 5 years with Slack, this one changed how I used it the most.  Now I can have more than just the "starred" or "unstarred" groups I mention above.  The level of fine-grain control I can now have on what grabs my attention is what lets me find the time to do other, more important stuff like writing a blog post about how to use Slack, for instance.

## TL;DR my slack setup
This section is a _quick_ run down of my setup / how I use Slack.  I will also be updating this section as I add ideas.

* Only show unread conversations everywhere *except* starred conversations
* Star conversations I need, including channels and DMs: close DMs that aren't active, but leave them as starred.
* Multiple levels of "in" a channel ends up with prioritized groups in order:
  - Starred
  - Higher on the list (will typically be "above the fold"), not muted
  - Higher on the list, muted
  - Other, not muted
  - Other, muted
* Channel Groups & sidebar layout:
  - Starred
  - My Team
  - Friends (people in other teams I'm friends with)
  - Rest of Marketing (marketing department channels for not my department)
  - Company & Industry (company-wide and industry-specific channels)
  - Social (purely social channels, interest groups)
  - Help me help you (git help, MR help channels where I can help and channels I go to for help)
  - Customers (customer/account specific channels)
  - The rest of the channels
  - DMs
  - Apps
  - Group DMs somewhere?  Can I delete them forever?
* Keyboard shortcuts I love
  - Use `option` (or `alt`) + `shift` + up/down arrow constantly to jump to the next unread conversation
  - `cmd` (or `windows`) + `t` to jump to a channel or DM
  - `cmd` + `f` to search
  - `cmd` + `1` or `2` to switch between workspaces
* Search keywords like `in:#channelname` and `from:@Brendan` when I remember pieces of a message besides content that will help.
