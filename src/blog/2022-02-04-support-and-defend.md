---
title: "Support and defend the constitution against all enemies foreign and domestic"
date: 2022-02-04 01:00:00
tags: []
type: "post"
blog: true
author: Brendan
excerpt: 'There are only two ways that history will judge the current Republican Party and both start with "anti-democratic"'
meta:
  - name: "twitter:title"
    content: "Support and defend the constitution against all enemies foreign and domestic"
  - name: "twitter:description"
    content: 'There are only two ways that history will judge the current Republican Party and both start with "anti-democratic"'
  - name: "og:title"
    content: "Support and defend the constitution against all enemies foreign and domestic"
  - name: "og:description"
    content: 'There are only two ways that history will judge the current Republican Party and both start with "anti-democratic"'
---

<blockquote class="info">
  <p>
    NOTE: Sorry for the interruption in your regularly scheduled tech content. But I had to say something about a release that happened in my home country of the United States today
  </p>
</blockquote>

There are only two ways that history will judge the current Republican Party:

- As an anti-democratic party that lost and couldn't destroy the American experiment
- Or as an anti-democratic party that won and started the end of America. And with it, a slide away from democracy worldwide.

There is no other way to look at it. Anyone who looks at the events of January 6th and sees "legitimate political discourse" isn't for American democracy. It's not more complicated than that. January 6th saw a breach of the very halls of the Capitol building while Congress was serving its Constitutional duty. The Consitution is very clear on this:

> The President of the Senate shall, in the Presence of the Senate and House of Representatives, open all the Certificates, and the Votes shall then be counted. The Person having the greatest Number of Votes shall be the President.

Even if one thought there was somehow a legitimate political discussion on who the President should be. Doing so with violence that left five people dead, a history building forever changed, and saw Congresspeople with gas masks on is not a "legitimate" way to have that discourse.

Thus, anyone who would call that "legitimate political discourse" has given up on participating in the American political system as we know it. Being that we haven't faced this before - a major party who refuses to participate in the democracy which the Constitution defines - I don't know how to save it. But I would say that I have a pretty vested interest in protecting it for myself and my children.

The best way I can think of right now is to support the people doing the right thing. People who speak the truth. People who agree with the Consitution. People who don't support the violence of that day or try and reframe it as "legitimate political discourse." We all saw with our own eyes what happened on January 6th. No amount of propaganda from the GOP can erase that if we're very clear about what happened. And that involves stating clearly that violence has no place in our political discourse. And in getting to the bottom of what happened that day.

That's why I'm thankful and supportive of people like [Liz Cheney](https://twitter.com/RepLizCheney), [Adam Kinzinger](https://twitter.com/AdamKinzinger), [Michael Steele](https://twitter.com/MichaelSteele), and [Larry Hogan](https://twitter.com/LarryHogan). And you should be, too, if you're pro-American democracy. Regardless of what policy differences you may have, I think the only way out of this mess is to support those with who we disagree but who are clearly on the side of the truth, the Consitution, and America.
