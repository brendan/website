---
title: 'Building a COVID screening app for our school...in a week and a half'
date: 2020-11-08 05:00:00
author: brendan
type: post
blog: true
excerpt: >
  With about three weeks left until school was supposed to start, our school's principal was calling me with a problem...and I leveraged iteration to fix it.
meta:
  - name: 'twitter:title'
    content: 'Building a COVID screening app for our school...in a week and a half'
  - name: 'twitter:description'
    content: "With about three weeks left until school was supposed to start, our school's principal was calling me with a problem...and I leveraged iteration to fix it."
  - name: 'og:title'
    content: 'Building a COVID screening app for our school...in a week and a half'
  - name: 'og:description'
    content: "With about three weeks left until school was supposed to start, our school's principal was calling me with a problem...and I leveraged iteration to fix it."
---

The school year in 2020 is unlike ever before. Of course, in March, when the coronavirus lockdowns first hit the US, all schools shut down. And as the school year started in the fall again, many students are still doing virtual learning. In some cases - like that for my kid's school - schools have been able to reopen at a reduced capacity. And, of course, with heavily modified procedures, policies, and facilities.

One of those policies we knew would change was to screen students for possible infection before school. My wife even works at the school, but we were still unsure how that would look. The US CDC issued guidance about temperature, contact tracing, and symptoms. But the logistics of the process were entirely unknown to us for some time...that is, until I got a phone call in the middle of September.

With about three weeks left until school was supposed to start, our school's principal was calling me with a problem. The rest of the school system's plan for screening wasn't going to work for our school. Every day parents or guardians would have to answer three screening questions about their students, and the school would have to verify this was recorded before letting the students on campus. The plan was to do this through Google Forms and a Google Sheet with results every day. But with almost 800 students in our school, the principal was worried it was going to take half the school day just to get the students in the building. She knew that I was "in tech" and said, "can you write us an app." Of course, anything is possible. But for some reason, I said yes, knowing full well the deadline of being working, tested, parents enrolled, and rolled out to the whole school was next to impossible.

Rather than seeing this deadline as an impossible mountain to climb, I decided to use it as a chance to practice what I preach: rapid iteration, continuous deployment, and observability in production would all come together to make it happen. This is the origin story of [Knight Check](https://knightcheck.com/) - a COVID screening tool for schools, written in a week and a half and now used by thousands of parents and staff members.

## Getting Started: Boring Solutions

I knew that I didn't have time to write the perfect front end that looked pixel-perfect to any design that I would come up with in my head. I also knew I'd have to leverage as many libraries for the front to back end connection to ensure I could spend time writing the app logic and not gluing together a whole full-stack app. And as much as I love to learn new langauges and frameworks, I'd have to build this one with what I knew: [Vue.js](https://vuejs.org/).

Once I knew that Vue would be the main front-end framework, I immediately decided to use [Vuetify](https://vuetifyjs.com/) for all design. The beauty of Vuetify is it is based on material design and thus gives you a clean, professional design out of the box. I didn't have to spend a lot of time thinking about making the page look the way I wanted...Vuetify has components to create any basic web app. And in the end, I'm proud of how the app looks - even though I can't take any design credit...most of that credit goes to Vuetify.

Another complex decision - what about the database and other backend decisions? Here again, I chose what I know - I've built several [side projects](https://boleary.dev/side-projects/) on GCP's [Firebase](firebase.google.com/), and I knew it would give me enough to get the app going without too much extra work. Firebase provides a built-in NoSQL database with security modeling, authentication with oAuth to social networking platforms, and free web hosting. Considering that the school wasn't paying me for the app, this seemed ideal to me.

## Coding it: Great Beta Testers

When it came time to code, I was lucky in more than one way. I was going to a beach house with my family, parents, and siblings the week after saying "yes" to the job. Of the six adults in the house, 4 were educators. I spent nights coding changes after the kids had gone to bed, and Shark Week was on the TV and spent nap time and other times in between getting direct feedback and user-testing in real-time.

Basically, without them asking for it, I had turned the beach house into an incubator - allowing me to iterate quickly and get great feedback. This kind of feedback loop is critical to the development process. It reminded me how key it is to limit cycle time as much as possible to understand if the problem you're trying to solve with software is solving the problem your users need to solve.

Often, talking about a solution in a bubble or on a whiteboard can seem crystal clear, but there is dissonance and disagreement when the actual implementation time comes. Between miscommunication and users not even knowing what they want, the original proposal and the real solution can differ quite a bit. Thus, everything you can to [iterate quicky](https://about.gitlab.com/handbook/values/#iteration) and decrease the [cycle time](https://about.gitlab.com/handbook/values/#reduce-cycle-time) between changes will ensure you can skate to where the puck is going to be.

## Making it real: Continous Delivery

## Testing in Production: Observability
