---
title:  "Three keys to making the right multi-cloud decisions"
date:   2020-07-13 05:00:00
author: brendan
type: post
blog: true
excerpt: > 
  The question isn't about if you will be a multi- or hybrid-cloud company. The question is - are you ready to be better at it than your competition. 
meta:
  - name: "twitter:title"
    content: "Three keys to making the right multi-cloud decisions"
  - name: "twitter:description"
    content: "The question isn't about if you will be a multi- or hybrid-cloud company. The question is - are you ready to be better at it than your competition. "
  - name: "og:title"
    content: "Three keys to making the right multi-cloud decisions"
  - name: "og:description"
    content: "The question isn't about if you will be a multi- or hybrid-cloud company. The question is - are you ready to be better at it than your competition. "
---

> This blog was insipred by my talk [of the same name](/talks/2019-10-16-three-keys-to-multicloud-decisions.html).

In recent years, there has been a lot of discussion about the possibility of multi- and hybrid-cloud environments.  Many business and technology leaders have been concerned about vendor "lock-in" or an inability to leverage the best features of multiple hyper clouds.  In regulated industries, there can still be a hesitancy to move "everything" to the cloud, and many want to keep some workloads within their physical data centers.

<BlogImage
  image="/img/blog/jen-theodore-CiMITAJtb6I-unsplash.jpg"
  caption=""
/>

## Today's Reality

The reality in the enterprise is that multi- and hybrid-cloud is already here. A 2019 State of Cloud report showed 84% of organizations are already using multiple clouds today.  On average they used more than four clouds in those responses. At the same time, we know that software excellence is the new operational excellence. "Software has eaten the world," and our competitiveness depends on our ability to deliver better products faster.

Based on those realities, the question isn't about if you will be a multi- or hybrid-cloud company. The problem is: are you ready to be better at it than your competition?  If we accept that a multi-cloud strategy is required, we need to systemize our thinking.  There are three key enablers here to consider: workload portability, ability to negotiate with suppliers, and the ability to select the best tool for a given job.  The cloud promises to remove undifferentiated work from our teams. To realize that potential, we must have a measured approach.

## Workload Portability

The most critical enabler is workload portability.  No matter what environment your team is deploying to, we must demand the same level of compliance, testing, and ease-of-use.  Thus, creating a complete DevOps platform that is cloud-agnostic allows developers to create value without overthinking about where the code deploys.

## The Three Keys

In considering both the platform your developers will use and how to make the "right" multi-cloud decisions, there are three keys: visibility, efficiency, and governance.  

### Visibility

Visibility means having: information where it matters most, a trusted single source of truth, and the ability to measure and improve. Whenever considering a multi-tool approach - whether it is a platform for internal use or the external deployment of your applications - visibility is crucial.  For a DevOps platform, you want real-time visibility across the entire DevOps lifecycle.  For your user-facing products, observability and the ability to correlate production events across providers will be critical for understanding the system.

### Efficiency

Considering efficiency may be simple at first, but there are multiple facets to consider.  We must always be sure we are efficient for the right group.  If there is a tools team selecting tools, the bias may be to optimize for their team's efficiency.  But if a selection here saves the tools team of 10 people an hour a week but costs 1,000 developers even two extra minutes a month, there is a negative impact on efficiency. Our platform of choice must allow development, QA, Security, and Operations part of a single conversation throughout the lifecycle.

### Governance

And lastly, governance of the process is essential regardless of industry.  However, it has been shown that working this governance into the day-to-day processes team use allows teams to move quicker than a legacy "end of cycle" process. Embedded automated security, code quality, vulnerability management, and policy enforcement enable our teams to ship code with confidence.  Regardless of where the deploy happens, tightly control how code is deployed, and eliminate guesswork. Incrementally roll out changes to reduce impact, and ensure user authentication and authorization is enforceable and consistent.

## End Goal

These capabilities will help you operate with confidence across the multi-cloud and hybrid-cloud landscape.
