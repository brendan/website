---
title: Contact Brendan O'Leary
---

# Contact Me

If you would like to get in touch, please send a DM on [Twitter](https://twitter.com/olearycrew) or <br/>
an email at `brendan` [at] `olearycrew.com`.

## Social Channels

You can also reach me on these channels:

<div class="cards">
    <SocialCard
        icon="fab fa-gitlab"
        title="GitLab"
        link="https://gitlab.com/brendan"
    />
    <SocialCard
        icon="fab fa-twitter"
        title="Twitter"
        link="https://twitter.com/olearycrew"
    />
    <SocialCard
        icon="fab fa-mastodon"
        title="Mastodon"
        link="https://mastodon.social/@olearycrew"
    />
    <SocialCard
        icon="fab fa-linkedin"
        title="LinkedIn"
        link="https://www.linkedin.com/in/olearycrew/"
    />
</div>   


> If you don't hear from me within a few days, your email might have ended up in spam, and I'd recommend to reach out again via email or [Twitter](https://www.twitter.com/olearycrew).

## Schedule a Chat
<iframe src="https://app.calendso.com/brendan" frameborder="0" allowfullscreen height="600" width="100%"></iframe>

## Encrypted Chat
You can also start an encrypted chat with me [on Keybase.io](https://keybase.io/boleary/chat).

## Speaking
I enjoy speaking, teaching, and learning new things.  If you have an event you'd like to have me speak at, please reach out to me via one of the method's above.
