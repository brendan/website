---
title: Resume
---

# Brendan O'Leary Resume

## GitLab Inc.

> October 2017 - Present

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one vision: everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

#### Staff Developer Evangelist

Connect with other developers, contribute to open source, and share work externally about cutting-edge technologies on conference panels, meetups, in contributed articles and on blogs. Interact with developers across the globe at conferences as well as online, and co-create with the open-source community on the most impactful projects in the ecosystem. Focus on generating awareness about GitLab by rolling up my sleeves, contributing to the ecosystem, and enabling others to become evangelists outside the company as well.

#### Senior Product Manager, Verify (CI)

Define the vision and direction for the Verify stage of the GitLab application - including continuous integration, system, and usability testing. Managed the product lifecycle from end-to-end for Verify stage, partner with internal and external stakeholders to shape the future of DevOps as part of a fast-growing late-stage startup.

#### Manager, Professional Services

Lead a team of Implementation Engineers to deliver a high level of service to GitLab's large and strategic Fortune 500 clients. Work together with the other managers within the Customer Success organization to help execute on strategies and vision with the Director, WW Customer Success. Responsible for worldwide delivery of implementation, migration, integration and training services to a diverse set of customers. Created DevOps Transformation engagements to help customers level-up their software delivery across many verticals and industries.

## Slim.AI

#### Advisor

Helping Slim.ai build a strong, sustainable open source community of contributors, users, and advocates.

## Cloud Native Computing Foundation (CNCF)

#### Governing Board Member

The CNCF Governing Board (GB) is responsible for marketing and other business oversight and budget decisions for the CNCF. The GB does not make technical decisions for the CNCF, other than working with the TOC to set the overall scope for the CNCF.

## Orbis Technologies, Inc.

> October 2016 - October 2017

#### Director, Release Management & Software Quality

Build strong DevOps focused group within Orbis, a government and commercial contractor. Supported large programs for delivery to top-tier Fortune 100 companies and US Department of Defense, including United States Special Operations Command (SOCOM). Led adoption of continuous integration and continuous delivery methodologies both within Orbis and within the DoD itself.

## MagView

> June 2006 - October 2016

#### Director of Product

Focused on helping ASI grow their business in the healthcare technology field. Involved in managing all aspects of the company from support to implementation to new and current product development. Also placed in charge of many enterprise-level project management functions such as business development with well respected software vendors in the healthcare industry.

## Clyde Bergemann EEC

> June 2001 - June 2006

#### IT Consultant

Worked as a direct report to the CIO of EEC. Worked in all areas of IT including troubleshooting, networking, and enterprise server management. Helped to migrate a NT 4.0 domain to a 2003 Sever environment . Supported various departments including the engineering and sales divisions including end users as well as server support.

## Business Network Consulting

> July 2003 - August 2004

#### Systems Technician

Designed, implemented, and maintained a variety of Windows networks, servers, workstations, and networking equipment for a variety of clients. Consulted with various businesses on ways to improve their business model based on business requirements and technological needs.

## St. Mary's Schools

> 2002 - 2004

#### IT Consultant

Design and implement education IT solutions for a large private school in Annapolis, MD. Instrumental in upgrades to network infrastructure including roaming profiles, wireless networking and mobile computer labs.
