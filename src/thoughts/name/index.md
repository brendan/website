---
title: What's in a name?
meta:
  - name: "twitter:title"
    content: "What's in a name?"
  - name: "twitter:description"
    content: "Yes my last name has an apostrophe in it.  Yes that is a VALID name."
  - name: "og:title"
    content: "What's in a name?"
  - name: "og:description"
    content: "Yes my last name has an apostrophe in it.  Yes that is a VALID name."
---

# My Last Name is valid
My last name is `O'Leary`.  Spelling it over the phone is fun..."That's O, apostrophe, capital L, e a r y."  Despite what many developers think, this is an entirely valid last name.  I've dealt with folks too lazy to deal with a `'` in a last name my whole life, which is one thing.  I get it, SQL can be hard.  But then many "solve" this problem through form validation that says "please enter a valid last name."  Talk about *invalidating* someone's experience in life.

It's not the biggest deal, but I share because there are a LOT of less privileged people than me that deal with lots of ways that developers don't include them.  Names, for instance, [have a lot a variation](https://www.kalzumeus.com/2010/06/17/falsehoods-programmers-believe-about-names/) but it doesn't end there.  [Accessibility](https://brailleworks.com/accessibility-matters/) is critical.  Considering how your [terminology](/blog/2020-06-10-i-was-wrong-about-git-master.html) may impact other people is essential. Without diversity of thought in your development teams, you risk causing this same problem repeatedly for people with more pressing concerns.  So...don't be lazy.

## Examples
If you follow me on [Twitter](htttps://twitter.com/olearycrew), you'll sometimes see me post this when I find a new place my last name doesn't work.  I hope to encourage people to consider those who are different than themselves who will one day (hopefully, if you're successful) be users of their app.

To see those examples, you can search for [`#oapostrophe from:@olearycrew`](https://twitter.com/search?q=%23oapostrophe%20from%3A%40olearycrew&src=typed_query) on Twitter, or check out my collection below.

<a class="twitter-timeline" href="https://twitter.com/olearycrew/timelines/1280856165358985217?ref_src=twsrc%5Etfw">oapostrophe - Curated tweets by olearycrew</a> 