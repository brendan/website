---
title: "Contacting your Senators about Ukraine"
date: 2020-11-08 05:00:00
meta:
  - name: "twitter:title"
    content: "Contacting your Senators about Ukraine"
  - name: "twitter:description"
    content: "How to get in touch with your US Senators and what to say about the atrocities in Ukraine."
  - name: "og:title"
    content: "Contacting your Senators about Ukraine"
  - name: "og:description"
    content: "How to get in touch with your US Senators and what to say about the atrocities in Ukraine."
---

# Contacting your Senators about Ukraine

Are you watching the scary images coming out of Bucha and Kramatorsk and wondering what you can do to help? While you may feel powerless, contacting your US Senator directly can have a significant impact on the policy implemented: They need to know that "everyday" Americans are worried about what's happening and want our government to do more.

## How to contact

You can find your Senator's contact information on [this website](https://www.senate.gov/senators/senators-contact.htm). You can either email them or call their office directly. I believe that calling has the most significant impact (with no data to back this up). And if you don't want to talk to a human, you can call outside of their office hours and leave a message.

## What to say

The first best thing to say is precisely what you're feeling - it's okay for them to know you're upset or angry.

If you'd rather just read a script - that's great...write something. If you don't want to write something, I've included what I said to my Senators below.

## What I've said

Hello, this message is for Senator Cardin. My name is Brendan O'Leary, and I live in Annapolis.

I'm calling today about the illegal Russian invasion of Ukraine. The images coming out of Bucha and today from Kramatorsk show us clearly that Putin and the Russian army are committing atrocities against the civilian population in Ukraine. More urgently, what the international community has done thus far has NOT been enough to stop Russia from committing these war crimes.

I want to urge you and your colleagues in the Senate to do more. Please get in touch with President Biden and implore him to give the Ukrainians whatever they need in military assistance. And ask him to contact our allies in Europe and encourage them to stop the flow of Russian oil into the EU.

History has its eyes on us.  So do our children.  Now that we've seen what Putin is capable of, we will be judged by our children and history on what we do or do not do.

Thank you very much for your time; you can reach me at 443-xxx-xxxx if you have any questions.
