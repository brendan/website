---
title: "Closer to the Code"
subtitle: "Speed up development by focusing on CI"
talk: true
tagline: Performance in DevOps is just like software performance in general - get it closer to the processor (or code) and you'll be able to move a lot faster
abstract: |
    When we want to improve the speed and performance of our applications, we often look at moving processes “closer to the processor.” But, what if the speed we want to improve is our cycle time when shipping customer value? The answer is similar - move the processes required to ship closer to the code. When we invest in continuous integration (CI), we can plateau once we have the code building and tests running. However, to deploy effectively, we need to embrace code review, dependency assurance, security scanning and performance metrics for every commit. To accelerate this cycle, all of these required processes must happen closer to the code. Learn how Kubernetes and GKE - paired with GitLab CI/CD - make this promise of CI possible, all without writing a bunch of brittle code glue. 
#slides: https://docs.google.com/presentation/
#video: https://www.youtube.com/
#events:
#  - name: A Great Event
#    date: "2020-02-02"
#    href: https://www.youtube.com/
---

<Talk :talk="$page.frontmatter"/>
