---
title: The Asynchronous Enterprise
talk: true
tagline: "How do you build a $3 billion dollar company with zero offices and 1,200 team members in 65+ countries?  Well let me tell you..."
abstract: |
    The one question I'm asked more than any other when talking about working at GitLab is: wait, you don't have any offices? That is often followed by a confused look or the direct question: How?
        
    Writing down decisions, asynchronous communication, measuring results, not hours. Companies often aspire to these goals...however in an all-remote company, they aren't aspirational - they are requirements. GitLab has grown from 9 people in 2014 to over 900 people in 55 different countries with a valuation of almost $3 billion.

    In this talk, we'll discover some of the not-so-secret sauce that GitLab has leveraged to achieve this growth. On this journey, our values have remained the same. We value collaboration, results, efficiency, diversity & inclusion, iteration, and transparency. And we've done all that without having any office, headquarters, or anything that looks like one.
slides: https://docs.google.com/presentation/d/e/2PACX-1vSSNAupgO12xnbLb3OOpKheEAepX5N8eKJOzKN6RcmW3er17LSkdGW5COh9MMm09ZTqzmoeGmGIWJEb/embed
video: https://www.youtube.com/embed/xRro-dDsYPU
events:
  - name: JSConf Hawaii 2020
    date: "2020-02-05"
    href: https://www.youtube.com/watch?v=xRro-dDsYPU

  - name: GOTO Chicago 2020
    date: "2020-04-27"
    href: https://gotochgo.com/
---

<Talk :talk="$page.frontmatter"/>

