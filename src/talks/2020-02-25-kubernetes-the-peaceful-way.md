---
title: Kubernetes the peaceful way
talk: true
tagline: "How can we make it peaceful for developers and save them from having to go through “Kubernetes the hard way”?"
abstract: |
    Kelsey Hightower has said, “We'll enter another phase where you'll build a platform on top of Kubernetes...it won't be worth mentioning that Kubernetes is underneath because people will be more interested in the thing above.” While Kubernetes is a once-in-a-generation technology, soon the days of struggling to spin up nodes or even wrestling with what a pod is will be behind us. Everyone - from the hyper-scalers to the newest startups - tries to eliminate the need to understand K8s and instead just enjoy its promises. What might that future look like?

    How can we make it peaceful for developers and save them from having to go through “Kubernetes the hard way”? This talk will bring together a few CNCF projects - Kubernetes, Helm, Buildpacks & more - to show how to create a layer & UI on top of Kubernetes that abstracts away the complexity of each of those projects in the raw.
#slides: 
#video: 
#events:
#  - name: DevOps Day Baltimore 2018
#    date: "2018-03-22"
#    href: https://www.youtube.com/watch?v=POBAnk3nWos&feature=emb_title
---

<Talk :talk="$page.frontmatter"/>

