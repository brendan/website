---
title: Side Projects
---

# Side Projects

## Full scale apps

<PortfolioItem
    title="listMe"
    link="https://listme.chat/"
    description="listMe is a Slack add in used by teams all over the world. Keep lists of to-dos, discussion topics, or ideas with each person you chat with in Slack!"
    image="/img/p_listme.png"
/>

<PortfolioItem
    title="quizMe"
    link="https://getquizme.com/"
    description="Simple online flashcards that will help you learn anything."
    image="/img/p_quizme.png"
/>

## Open Source Projects

<PortfolioItem
    title="Hacker News Suite"
    link="https://hnsuite.com/"
    description="An open source, unofficial extension to make Hacker News a little nicer."
    image="/img/p_hnsuite.png"
    :extralinks="[
        {text: 'Get the extention', link: 'https://chrome.google.com/webstore/detail/hacker-news-suite/hoaajkgemckdcpffabpncllfmeohodph'},
        {text: 'Checkout the open source repository', link: 'https://gitlab.com/brendan/hnsuite/'},
    ]"
/>

## Website design

<PortfolioItem
    title="Learning Tableau.com"
    link="https://learningtableau.com/"
    description="Full website redesign that lead to a 250% increase in sales month-over-month."
/>

<PortfolioItem
    title="Definotily.com"
    link="https://definotily.com/"
    description="Impulse domain purchase based on a silly slack conversation."
/>

## Podcast

I'm also the co-host of a podcast about art's role in society today called [Do You Even Art?](https://doyouevenart.com/)

## Home Automation

One of the fun things I like to do with technology is to make my life simpler at home through automation.

For example, working from home with four small children can be especially challenging. To handle this, I use a color-changing light bulb outside of my office, tied to my Alexa so that I can show red, yellow, or green to the outside world. That way (in theory, at least), I can take important calls undisturbed by a [tiny human](https://www.youtube.com/watch?v=m3ktsl6_Rpg).

I also used GitLab CI/CD to automate updates to my local DNS server and my RetroPi gaming system.

## Photography

A passion of mine in my spare time is amatuer photography. Taking pictures of landscapes or my kids.

![](/img/p_photography.jpg)
